var http = require('http');
var fs = require('fs');
var express = require('express');
var cheerio = require('cheerio');
var app = express();

var host = 'http://img.hb.aicdn.com/'; // 花瓣

app.use(express.static('public'));

app.get('/', function(req, res) {
    res.sendFile(__dirname + "/pic.html");
})

app.get('/getImages', function(req, res) {
    var targetUrl = req.query.url;
    downloadPage(targetUrl, function(data) {
        if (data) {
            var $ = cheerio.load(data);
            $('script').each(function(i, item) {
                var content = $(item).text();
                if (/app\.page\["board"\]/.test(content)) {
                    var listJson = content.match(/app\.page\["board"\] = ({.*})/)[1];
                    var pins = JSON.parse(listJson).pins;
                    for (var i = 0, len = pins.length; i < len; i++) {
                        var filename = pins[i].file.key + '_fw658';
                        var filetype = pins[i].file.type.split('/')[1];
                        downloadFile(host + filename, __dirname + '/download/' + filename + '.' + filetype, function(file) {
                            console.log('load file success');
                        })
                    }
                }
            })
        } else {
            console.log('获取失败');
        }
    })
})

var downloadPage = function(url, callback) {
    http.get(url, function(res) {
        var data = '';
        res.on('data', function(chunk) {
            data += chunk;
        })
        res.on('end', function() {
            callback(data);
        })
    }).on('error', function() {
        callback(null);
    })
}

var downloadFile = function(url, localFile, callback) {
    console.log(url);
    var writeStream = fs.createWriteStream(localFile);
    writeStream.on('finish', function() {
        callback(localFile);
    });
    http.get(url, function(res) {
        res.pipe(writeStream);
    })
}


var server = app.listen(8181, function() {
    var host = server.address().address;
    var port = server.address().port;
})